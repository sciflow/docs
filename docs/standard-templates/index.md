# Templates

Our standard templates available are **Generic Monograph** and **Generic
Journal**. Select one in the editing UI and you can modify standard settings
like paper size and headers.

Templates are available on standard installations by default. If you run your
own instance you can
[copy them](https://gitlab.com/sciflow/development/-/tree/main/templates) into
your own [template directory](/self-hosting/#customizing-templates).

You can check a template's configuration.sfo.yml file to see its default
configuration but it is easier to use the UI and navigate to the meta data
options as shown in the figure below.

<figure markdown>
  ![Settings UI showing a multiple options to customize a template](../standard-templates/customizing-templates.png){ width="400" }
  <figcaption>
        <b>Settings UI example</b>
        <p>When creating a new template, developers can choose to make options like fonts, margins, etc. available to end users<p>
    </figcaption>
</figure>

## Customizing the standard options

The configuration options for standard templates are saved alongside the document when you save it. To setup different defaults, you can copy an existing template and change the configuration.sfo.yml

