# Structure of a template

Extending existing templates means you can use and existing format. We will use a PDF for a journal article as an example on this page. You can check out the complete example [here](https://gitlab.com/sciflow/development/-/tree/development/templates/generic-journal).

!!! note "Required knowledge"
    The knowledge required to work on these templates will depend on the export format used (XML/PDF/HTML) but generally include work with JSON Schema 4 and TypeScript. For PDF templates you should be familiar with the [CSS Paged Media Spec](https://www.w3.org/TR/css-page-3/)

## Basic setup

Any template starts with a [configuration file](https://gitlab.com/sciflow/development/-/blob/development/templates/generic-journal/configuration.sfo.yml). The file is usually a YAML file that contains a number of documents, each containing one configuration section.

```yaml
kind: Configuration
spec:
  title: Generic journal template
  description: A simple one column journal template
  type: journal
  readme: |
    # About

    This template will provide a basic one column PDF export with a choice of citation styles.

  citationStyle:
    id: apa # any zotero citation style ID will work
---
kind: JournalTitle
page: body
spec: {}
---
kind: Page
page: body
spec:
  page:
    font:
      family: Arial
      size: 10pt
    size: A4
    margins:
      inner: 25mm
      outer: 18mm
    handling: single-sided
  typesetting:
    widows: 2
    orphans: 3
```

The format of this configuration (e.g. a section that has a **kind:**) corresponds to the schema exposed by the [components](https://gitlab.com/sciflow/development/-/tree/development/libs/export/src/html/components) responsible for rendering the document (basically a typescript interface with the same name as the configuration **Kind**). This schema can also displayed as a UI when documents are exported to allow authors or editors to make last minute changes to the configuration.

!!! tip "Kind"
    Each kind can only exist once per configuration. If two exist, the latter one will be used. This is usefull when we want to extend two templates (by just concatenating their configurations).

    Each kind, represented by it's implementing component will produce Paged Media CSS as well as DOM (if so required) that is injected into the document at the position of the Kind in the configuration. This way the **Kind:  JournalTitle** from the example above, generates a title and authors section at the beginning of the document that comes with its own styling for that section. As you can see in the [JournalTitle](https://gitlab.com/sciflow/development/-/blob/development/libs/export/src/html/components/journal-title/journal-title.component.tsx) component, the code to do this can use any custom logic that is required.

In addition, each template may also contain a meta data section in form of a [JSON Schema](https://json-schema.org/) (in YAML):

```yaml
type: object
title: Journal Meta Data
description: Contains additional meta data for the publication
properties:
    publication:
        type: object
        title: Publication data
        description: Basic publication information
        properties:
            doi:
                type: string
                title: DOI
```

If we wanted to add additional styling, a styles.[scss](https://sass-lang.com/) file can be added.

To get started, simply copy one of the template folders and modify the configuration as needed.