# Types of templates

Using the TKD you can setup new output formats from scratch or built on existing ones provided by other users. There are multiple levels depending on what how much customization is needed:

1. **Click and go**. Use a standard template and customize it using the template's settings. Then run with template with one of the hosted exporters (like PagedJS) to receive an export. This method does not require any technical setup.
    <figure style="border: 2px dotted #555; margin: 1rem;">
        <img src="../tdk/click.png" aria-label="Settings UI showing a multiple dropdowns for template options" />
        <figcaption style="text-align: left; margin: 0; padding: 1rem; font-size: 90%;">
            <b>Settings UI example</b>
            <p>When creating a new template, developers can choose to make options like fonts, margins, etc. available to end users</p>
        </figcaption>
    </figure>
1. **Customize existing**. Copy and customize the source code for an existing template using the documentation. You can run these templates with one of the standard exports (like PagedJS). Existing exports as HTML or XML can be adapted to fit your needs.

!!! note "Requirements"
    To work on these templates you will need to set-up your own development environment. To learn more see our [template documentation](template.md).

1. **Bring your own**. Bring your own tools and use the existing schema and libraries to create a publication pipeline.
