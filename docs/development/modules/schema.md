# Document Schema

The document schema used for SciFlow is based on the ProseMirror Schema definition outlined [here](https://prosemirror.net/docs/guide/#schema). It defines block and inline level nodes that may be part of the document.

!!! info "Meta data"
    The document itself may require more metadata (like authors and affiliations or figures) which are kept in a separate data structure. The entire format is described in the [*SFManuscriptFile*](https://gitlab.com/sciflow/development/-/blob/main/libs/schema/src/types.ts#L50)

To understand [the schema being used here](https://gitlab.com/sciflow/development/-/blob/main/libs/schema/src/schema.ts) better, the [ProseMirror schema guide](https://prosemirror.net/docs/guide/#schema) is a good primer.

An example document with a single heading and paragraph will look like this:

``` javascript
{ 
    type: 'doc',
    content: [
        {
            type: 'heading',
            attrs: { level: 1 },
            content: [ { type: 'text', text: 'A heading 1' } ]
        },
        {
            type: 'paragraph',
            content: [ { type: 'text', text: 'Some text.' } ]
        }
    ]
}
```

It uses the types (also found in the [*SFNodeTypes*](https://gitlab.com/sciflow/development/-/blob/main/libs/schema/src/types.ts#L4)) defined in the ProseMirror schema, and will be stored as *document* inside the *SFManuscriptFile*.

For an example of how to transform a document into an export format, see the [defaultHTMLTemplate](https://gitlab.com/sciflow/development/-/blob/main/tdk/src/html.ts#L7)