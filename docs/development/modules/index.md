# Introduction

SciFlow modules are published as installable packages and can be used to create publication formats not included so far.

## @sciflow/tdk

The Template Development Kit (TDK) allows custom production pipelines. It includes default implementations for XML and HTML export (which can also be used to produce PDFs with CSS Paged Media). The TDK also offers base styles needed for all CSS Paged Media documents. [Read more](https://gitlab.com/sciflow/development/-/tree/main/tdk)

## @sciflow/schema

The ProseMirror schema in this module contains everything that is needed to parse or serialize a scholarly document in the SciFlow document format. It builds the base for the editor and the structure for any ex- or import. You can read more about the schema [here](schema.md).

## @sciflow/import

The import library takes a document (like DocX or ODT) in [Pandoc](https://www.pandoc.org) AST JSON and writes it into our internal format (that can then be used by the editor and exporters). [Read more](https://gitlab.com/sciflow/development/-/tree/main/import)

## @sciflow/export

This library contains everything needed to transform the SciFlow document into any other format like JATS XML, PDF etc. There are currently two writers: XML and HTML. If you wanted to create your own exports, this is where you'd take inspiration. [Read more](https://gitlab.com/sciflow/development/-/tree/main/export)

## @sciflow/support

The support library provides helpers around working with files (like reading from S3). [Read more](https://gitlab.com/sciflow/development/-/tree/main/support)
