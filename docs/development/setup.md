# Dev setup

!!! info "Programming knowledge required"

    While you can easily change configurations of existing templates or copy them, the development of the TDK itself requires programming knowledge, especially:
    
    * Typescript
    * [CSS Paged Media](https://www.w3.org/TR/css-page-3/)

Development needs Pandoc to be setup as well as a Unix shell. If you are using Windows, a virtual machine with your favorite Linux distribution is recommended.

If you do not want to setup a local environment, a service like Gitpod can be used to develop in the browser. Our [default configuration](https://gitlab.com/sciflow/development/-/blob/development/.gitpod.yml) for the service can be found here. This can be a starting point if you are setting up a similar environment.

<video width="100%" controls="controls" preload="false">
    <source src="/development/gitpod-workflow.webm" type="video/webm">
    Your browser does not support the video tag.
</video>
