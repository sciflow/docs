# Reference app

The reference app provides on possible approach towards building an app based on our open components. It is possible to use different frameworks but this will require a deep understanding of the mechanisms used here. A basic knowledge of [Angular](https://angular.io) will be needed for some of the terminology.

![Mockup](mockup.svg)

## Base layout

SciFlow components like the editor or author management could theoretically be placed anywhere in an Angular app. The reference app aims to provide an oponionated layout that has worked well for SciFlow and introduces a shared terminology for the regions of the layout.

![App layout](layout.svg)

## Topnav
The topnav lives within the main layout. It is wrapped by an optional app menu that [slides in](https://material.angular.io/components/sidenav/overview) from the side.

## Main

The area below the top nav could be split depending on the use case. For the editor it would contain two regions represented by [router outlets](https://angular.io/api/router)

### Editor

The editor loads a document from the [store](https://ngrx.io/guide/store) and displays it for editing. All needed UIs to interact with the document are either placed inside the [ProseMirror](https://prosemirror.net/) editor instance or created as overlays (like modals) on top of the editor. The goal of overlays should be to not cover important components (e.g. for footnote modals not to cover the text where the footnote is placed or the DCA with the reference list).

### Document Context Area (DCA)

The dca is designed to provide context on what a user is doing with the document. This could be a reference list from which sources could be dragged into the text or a table of contents component that enables cross referencing. The DCA components may be switched through the DCA nav component next to the scrollbar.

In its initial state the DCA has a defaul width of 24rem but can be expanded to full screen with if needed. User resizing of the dca is currently not planned. When on mobile the collapsed DCA should be rendered full width as well so the editor is hidden completely.

![DCA layout](dca.svg)
