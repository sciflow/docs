# Submission templates

To make the submission easier, we offer a badge that journals can integrate onto their homepages.

<a href="https://app.sciflow.net/manage/create/sciflow-thesis?utm_medium=badge&utm_term=submission-template" target="_blank" title="Create document">
    <img src="/publish/submission-template.svg" alt="Submission template" />
</a>

Below example creates a document for *sciflow-thesis*. You can receive the slug from SciFlow support.

```
<a href="https://app.sciflow.net/manage/create/sciflow-thesis?utm_medium=badge&utm_term=submission-template" target="_blank" title="Create document">
    <img src="https://docs.sciflow.org/publish/submission-template.svg" alt="Submission template" />
<a>
```
