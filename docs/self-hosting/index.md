# Self-Hosting

!!! info "Hosted demo"
    We are hosting a demo at [os-aps.sciflow.net](https://os-aps.sciflow.net/)

    The demo is using the :latest images. To get access to the current development version (which may not work at all times), you can access [os-aps-next.sciflow.net](https://os-aps-next.sciflow.net/)

Please feel free to ask any questions in the #development channel over on [Slack](https://join.slack.com/t/scifloworg/shared_invite/zt-xlht19cx-DhTGSYAkNyDP5wAbLQvxpQ).

## Running the App

To run the app locally, create a new directory for the data, navigate to it in
your terminal, and use Docker to set up the container. Files such as manuscripts
and configuration data will be stored in this directory. Below is a step-by-step
guide:

### 1. Start the Server with Docker

Run the following command to start the server on port 3000:

```sh
docker run \
    --rm \
    -p 127.0.0.1:3000:3000/tcp \
    --platform linux/amd64 \
    --name os-aps-demo \
    --volume "$(pwd):/data" \
    --env LOCAL_STORAGE_PATH=/data/manuscripts \
    --env INSTANCE_TITLE="Personal Demo Instance" \
    --env FONT_PATH=/data/fonts/ \
    --env LOG_LEVEL=debug \
    # if you want to transform wmf/emf files --env TRANSFORM_IMAGE_URL=http://localhost:3001 \
    registry.gitlab.com/sciflow/development/server:latest
```

Remove the transform image URL if you are not running that service on 3001 (see below).

- **Parameters**:
  - `--volume "$(pwd):/data"` mounts the current directory for persistent
    storage.
  - `--env INSTANCE_TITLE` sets the instance title visible in the app.
  - `--env LOCAL_STORAGE_PATH` specifies where manuscript files will be stored.
  - `--env FONT_PATH` defines the location of fonts for document rendering.
  - `--env TRANSFORM_IMAGE_URL` connects the app to a transformation service for
    images.

Once the server starts, open [http://localhost:3000](http://localhost:3000) in
your browser to access the app.
### 2. Optional: Customize Your Domain

If you plan to access the app from a specific domain, add this line to your
`docker run` command:

```sh
--env INSTANCE_URL="https://yourdomain:port"
```

This ensures compatibility with browsers that enforce Cross-Origin Resource
Sharing (CORS) policies.

### 3. Customize Templates

To use custom export templates, follow these steps:

1. Create a `templates` directory inside your data directory.
2. Download or create templates and place them in the `templates` folder.
3. Modify the `docker run` command to include the following environment
   variable:

   ```sh
   --env TEMPLATE_SOURCE=/data/templates \
   ```

4. Reload your export to see changes reflected.

Example template repository:
[Generic Monograph Template](https://gitlab.com/sciflow/development/-/tree/main/templates/generic-monograph).

Templates are evaluated during export, allowing you to test changes without
restarting the server.

### 4. S3 Storage Configuration (Optional)

If you wish to use S3 for file storage, specify the following:

```sh
--env S3_ENDPOINT="your-s3-endpoint" \
--env S3_ACCESS_KEY="your-access-key" \
--env S3_SECRET_KEY="your-secret-key" \
--env S3_REGION="your-region" \
--env S3_IMPORTER_BUCKET="your-bucket-name" \
```

### 5. Using PowerShell on Windows

If you’re using PowerShell, replace line breaks in the `docker run` command with
backticks (`\``):

```powershell
docker run `
    --rm `
    -p 127.0.0.1:3000:3000/tcp `
    --platform linux/amd64 `
    --name os-aps-demo `
    --volume "${PWD}:/data" `
    --env LOCAL_STORAGE_PATH=/data/manuscripts `
    --env INSTANCE_TITLE="Personal Demo Instance" `
    --env FONT_PATH=/data/fonts/ `
    --env TRANSFORM_IMAGE_URL=http://localhost:3001 `
    registry.gitlab.com/sciflow/development/server:latest
```

### 6. Debugging and Logs

To debug issues, check the container logs using:

```sh
docker logs os-aps-demo
```

If needed, add `-it` and `bash` to the `docker run` command to open a shell
inside the container for troubleshooting.

### 7. Image Transformation Sidecar (Optional)

If you are using a sidecar container for image transformations, run the following Docker container alongside your main application. The sidecar uses LibreOffice to transform WMF and EMF files into PDF or PNG formats. It supports additional processing such as cropping whitespace from PDFs using pdfcrop and extracting images as PNGs using pdftoppm. The sidecar ensures compatibility with various input formats by leveraging robust file conversion and transformation tools.

```sh
docker run \
    --rm \
    -p 127.0.0.1:3001:3001/tcp \
    --name file-transform-sidecar \
    registry.gitlab.com/sciflow/development/file-transform-sidecar:latest
```

- Update the `TRANSFORM_IMAGE_URL` in the main app to `http://localhost:3001`.

## Development Setup

If you’d like to go beyond customizing templates (e.g., modify how exports are
rendered or debug the backend), see the [Development Setup Guide](/development/setup).

This updated version ensures users understand how to deploy the app using Docker
without relying on Knative, providing clear instructions for configuration,
customization, and debugging.

## Troubleshooting: Proxy Configuration Issues

!!! note "External Infrastructure Support"
    We cannot provide support for all possible proxy configurations. However, we are collecting insights from OS-APS users who have encountered specific issues and solutions. We're happy to share these here, but please note that we cannot assist with infrastructure setups beyond our control.

If you are running OS-APS behind a reverse proxy such as **Apache** or **NGINX**, be aware that **encoded slashes (`%2F`) in URLs** may cause unexpected `404 Not Found` errors. This happens because some proxies decode or block encoded slashes by default, which can interfere with how the application processes file paths.

### Why Does This Happen?
The application **internally encodes slashes** (`/` → `%2F`) in certain URLs to maintain structure when passing paths as parameters. However, some proxies **automatically decode `%2F` into `/`** or outright **reject** requests containing encoded slashes.

This can lead to:
- **404 errors** if the proxy sends the wrong request format to the backend.
- **Security rejections** if encoded slashes are not explicitly allowed.

### How Different Proxies Handle This
- **Apache**: By default, Apache rejects requests containing `%2F`. This behavior can be adjusted through configuration options.
- **NGINX**: NGINX decodes `%2F` before forwarding, which can break path-based routing if the backend expects the encoded format.

### Fixing the Issue
To avoid problems:
1. Ensure the **reverse proxy forwards URLs exactly as received**, without decoding `%2F`.
2. Adjust proxy settings to allow encoded slashes **without decoding them** before reaching the backend.
3. If you see unexpected 404s for URLs containing `%2F`, check whether your proxy is modifying the request.

Since every infrastructure setup is different, refer to your proxy’s documentation for specific configurations.
