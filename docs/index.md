# Welcome

Part of our mission to enable scholar-led publications is to make our data formats and transformation tools available to you to adapt and build on. This documentation collects all the resources you need to get started.

## Where to start?

If you want to publish a paper or monograph but have either *very little time* or are still *new to academic publishing* (welcome!), please check out our [standard templates](/standard-templates). These templates were developed as part of the OS-APS project and can be used with the [hosted demo](https://os-aps.de/demo).

If you're ready to dive in deeper, please feel free to [set up your own instance](self-hosting) of the software and customize your templates.

In case you already have a document from SciFlow but would like to transform it, check out our [helper libaries](/development) to do that.

!!! tip "Ask us for help"
    We offer custom templates, support and more through [SciFlow Publish](https://sciflow.net/publish). If you have an existing publication or would like to create a new one with technical support, please don't hesitate to reach out.

## What is the difference between this and SciFlow.net?

[SciFlow.net](https://www.sciflow.net) is our hosted academic writing and publishing platform. It uses some of the same technologies as shown here but has a number of additional features for collaboration, publishing, and more.